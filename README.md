# GitLab Pages - Swagger UI
![CI/CD](https://gitlab.com/pkng-demos-bar/components/swagger-pages/badges/main/pipeline.svg)
![Release](https://gitlab.com/pkng-demos-bar/components/swagger-pages/-/badges/release.svg)

This CI/CD component provides a quick template to host Swagger UI on GitLab Pages, which can serve as the API documentation for your development team

---

## Usage
This component provides a job for deploying of generated Swagger API specification into GitLab Pages[[1]] and is configured to triggered by default on Git tag event, force deployment can be configured via input 'FORCE_DEPLOY'.

## Example

```yaml
include:
  # include the component located in the current project from the current SHA
  - component: gitlab.com/pkng-demos-bar/components/swagger-pages@<version>
    inputs:
      STAGE: "deploy"
      DOCS_FOLDER: "api-docs"
      SPEC_TO_DISPLAY: "swagger.yaml"
```

## Components Inputs

| INPUT | DEFAULT | DESCRIPTION |
| --- | --- | --- |
| STAGE | deploy | The stage name of the job to deploy GitLab Pages. |
| DOCS_FOLDER | api-docs | Folder containing the generated Swagger API Specifications |
| SWAGGER_UI_VERSION | v5.9.0 | Swagger UI version |
| SPEC_TO_DISPLAY | swagger.yaml | OpenAPI 3.0 specification file |
| FORCE_DEPLOY | false | Set to `true` to force a deployment when not creating a tag. |


## References
GitLab Pages Documentation [[1]]  
GitLab Pages Swagger UI Template for Pages [[2]]  

[1]:https://docs.gitlab.com/ee/user/project/pages/ "GitLab Pages Documentation"
[2]:https://gitlab.com/gitlab-org/gitlab-foss/-/edit/master/lib/gitlab/ci/templates/Pages/SwaggerUI.gitlab-ci.yml "GitLab Swagger UI Template for Pages"
[10]:https://gitlab.com/mdhtr/gitlab-pages-swagger-ui
[11]:https://github.com/peter-evans/swagger-github-pages